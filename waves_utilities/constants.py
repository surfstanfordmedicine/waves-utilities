"""Constants for use elsewhere in the utility codebase
"""

# Extreme 16-bit integer range values are used to encode various error and NaN types by
# the source Philips bedside monitor system
BAD_VALUES = [-32768, -32767, 32767, 32768]
