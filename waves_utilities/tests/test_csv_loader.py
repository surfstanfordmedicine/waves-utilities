import os
import pytest

import unittest
import unittest.mock as mock
import numpy as np

from waves_utilities.util import get_waves_root
from waves_utilities import WavesUtilityAPI
from waves_utilities.tests.fixtures import *


class TestCSVLoader:
    def test_read_csv(self, util_api: WavesUtilityAPI, waveform_test_csv: str):
        # Load all rows from .csv (unspool from generator to list)
        csv_rows = list(util_api.csv_manager.read_csv(waveform_test_csv))

        # Check that all data was loaded
        assert len(csv_rows) == 3

        # Check that all fields are present and match expected dtypes
        csv_row = csv_rows[0]
        assert isinstance(csv_row["wave_id"], str)
        assert isinstance(csv_row["group"], int)
        assert isinstance(csv_row["frequency"], int)
        assert isinstance(csv_row["gain"], float)
        assert isinstance(csv_row["type"], str)
        assert isinstance(csv_row["units"], str)
        assert isinstance(csv_row["start_index"], int)
        assert isinstance(csv_row["stop_index"], int)
        assert isinstance(csv_row["waveform"], np.ndarray)
