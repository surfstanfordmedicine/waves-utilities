import os
import pytest

from waves_utilities.util import get_waves_root
from waves_utilities import WavesUtilityAPI


@pytest.fixture
def util_api():
    return WavesUtilityAPI()


@pytest.fixture
def waveform_test_csv():
    """Path to small-scale waveform .csv file used for testing.

    Note:  Can create a new version of this with scripts/create_test_sample.py
    """
    yield os.path.join(get_waves_root(), "tests/data/waveform_test_extract.csv")
