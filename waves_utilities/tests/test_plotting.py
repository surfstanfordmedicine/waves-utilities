from waves_utilities.tests.fixtures import *


class TestCSVLoader:
    def test_plot_waveform(self, util_api: WavesUtilityAPI, waveform_test_csv: str):
        """Plot a single waveform to file"""
        # Set file path and delete existing file
        plot_filepath = os.path.join(
            get_waves_root(), "tests/plots/single_waveform.png"
        )
        if os.path.exists(plot_filepath):
            os.remove(plot_filepath)
        assert not os.path.exists(plot_filepath)

        # Load single row from .csv (unspool from generator to list)
        csv_row = next(util_api.csv_manager.read_csv(waveform_test_csv, limit=1))

        # Plot, save to file, and check
        util_api.waveform_plotter.plot_waveform(
            csv_row, save_filepath=plot_filepath, duration_seconds=5
        )
        assert os.path.exists(plot_filepath)

    def test_plot_waveforms(self, util_api: WavesUtilityAPI, waveform_test_csv: str):
        """Plot multiple waveforms to file"""
        # Set file path and delete existing file
        plot_filepath = os.path.join(
            get_waves_root(), "tests/plots/multi_waveforms.png"
        )
        if os.path.exists(plot_filepath):
            os.remove(plot_filepath)
        assert not os.path.exists(plot_filepath)

        # Load all rows from .csv (unspool from generator to list)
        csv_rows = list(util_api.csv_manager.read_csv(waveform_test_csv))

        # Plot, save to file, and check
        util_api.waveform_plotter.plot_waveforms(
            csv_rows, save_filepath=plot_filepath, title="Test Waveform Data"
        )
        assert os.path.exists(plot_filepath)
