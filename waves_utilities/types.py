"""Custom data types, primarily for type hinting and autocompletion
"""
import datetime
import numpy as np
from typing import TypedDict


class WaveformCSVRow(TypedDict):
    """Typed representation of the data contained in each row of the waveform .csv files
    downloaded from the Redivis data store
    """

    wave_id: str  # Unique sample identifier, roughly analagous to hospital encounter id
    group: str  # Subgroup of data within a single sample (for partitioning on size)
    type: str  # Waveform type
    units: str  # Units of measure for the waveform
    frequency: int  # Sample frequency of the waveform (in Hz)
    gain: float  # Gain factor, multiply to convert the waveform to appropriate units
    start_index: int  # Start index of selected waveform data within the raw array
    stop_index: int  # Stop index of selected waveform data within the raw array
    start_datetime: datetime.datetime  # Start datetime of the waveform data (not provided for anonymized samples)
    stop_datetime: datetime.datetime  # Stop datetime of the waveform data (not provided for anonymized samples)
    waveform: np.ndarray  # Raw unscaled waveform data array
