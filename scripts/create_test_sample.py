"""Create a small-scale test sample data file from a full source .csv
"""
import os
from waves_utilities.util import get_waves_root
from waves_utilities import WavesUtilityAPI

# Conversion parameters
num_samples = 3
num_seconds = 100
input_filepath = os.path.join(get_waves_root(), "../data/WAVES_demo_extract_v1.csv")
output_filepath = os.path.join(get_waves_root(), "tests/data/waveform_test_extract.csv")


print(
    f"Subsetting to {num_samples} samples of {num_seconds} seconds each from "
    f"{input_filepath} to {output_filepath}"
)
util_api = WavesUtilityAPI()
small_rows = []
for row_index, row in enumerate(util_api.csv_manager.read_csv(input_filepath)):
    # Pick out the samples to include in the test subset
    # if row_index in [1]: continue  # all zeros, skip
    if row_index not in [2, 11, 14]:
        continue

    # Clip to short duration and add to samples
    stop_index = row["frequency"] * num_seconds
    row["stop_index"] = stop_index
    row["waveform"] = row["waveform"][:stop_index]
    small_rows.append(row)

    # if row_index == num_samples - 1:
    #     break


util_api.csv_manager.write_csv(output_filepath, small_rows)

# Validate:
print(f"Re-reading the new .csv from {output_filepath}")
for row_index, row in enumerate(util_api.csv_manager.read_csv(output_filepath)):
    for key, value in row.items():
        print(f"{key} ({type(value)}): {value}")
